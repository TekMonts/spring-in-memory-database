package com.quocdao.inmemory.database;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication()
@Controller
@EntityScan("com.quocdao.inmemory.database.entity")
@EnableJpaRepositories("com.quocdao.inmemory.database.repository")
public class InMemoryDBApplication {
	@GetMapping("/")
	public String index() {
		return "index";
	}

	public static void main(String[] args) {
		SpringApplication.run(InMemoryDBApplication.class, args);
	}
}