package com.quocdao.inmemory.database.controller;

import com.quocdao.inmemory.database.entity.PersonH2;
import com.quocdao.inmemory.database.service.H2JDBCService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
public class H2JDBCController {

    @Autowired
    private H2JDBCService h2JDBCService;

    @GetMapping("/jdbc/h2/{id}")
    public ResponseEntity<?> getByIDFromH2DB(@PathVariable("id") long id) {
        log.info("getByIDFromH2DB(" + id + ")... CALLED!");
        return h2JDBCService.getPersonUsingH2(id);
    }

    @GetMapping("/jdbc/h2")
    public ResponseEntity<?> geAllFromH2DB() {
        log.info("geAllFromH2DB()... CALLED!");
        return h2JDBCService.getAllPersonUsingH2();
    }

    @PostMapping("/jdbc/h2")
    public ResponseEntity<?> postToH2DB(@RequestBody PersonH2 personH2) {
        log.info("postToH2DB(" + personH2.toString() + ")... CALLED!");
        /*personH2.setCreateTime(new Date());
        personH2.setLastUpdated(new Date());*/
        return h2JDBCService.postPersonToH2(personH2);
    }

    @PatchMapping("/jdbc/h2")
    public ResponseEntity<?> patchToH2DB(@RequestBody PersonH2 personH2) {
        log.info("patchToH2DB(" + personH2.toString() + ")... CALLED!");
        return h2JDBCService.updatePersononH2(personH2);
    }

    @DeleteMapping("/jdbc/h2/{id}")
    public ResponseEntity<?> deleteFromH2DB(@PathVariable("id") long id) {
        log.info("deleteFromH2DB(" + id + ")... CALLED!");
        return h2JDBCService.deletePersonFromH2(id);
    }
}
