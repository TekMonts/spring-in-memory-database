package com.quocdao.inmemory.database.controller;

import com.quocdao.inmemory.database.entity.PersonH2;
import com.quocdao.inmemory.database.service.H2JPAService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
@Slf4j
public class H2JPAController {

    @Autowired
    private H2JPAService h2JPAService;

    @GetMapping("/jpa/h2/{id}")
    public ResponseEntity<?> getByIDFromH2DB(@PathVariable("id") long id) {
        log.info("getByIDFromH2DB(" + id + ")... CALLED!");
        return h2JPAService.getPersonUsingH2(id);
    }

    @GetMapping("/jpa/h2")
    public ResponseEntity<?> geAllFromH2DB() {
        log.info("geAllFromH2DB()... CALLED!");
        return h2JPAService.getAllPersonUsingH2();
    }

    @PostMapping("/jpa/h2")
    public ResponseEntity<?> postToH2DB(@RequestBody PersonH2 personH2) {
        log.info("postToH2DB(" + personH2.toString() + ")... CALLED!");
        personH2.setCreateTime(new Date());
        personH2.setLastUpdated(new Date());
        return h2JPAService.postOrUpdatePersonToH2(personH2);
    }

    @PatchMapping("/jpa/h2")
    public ResponseEntity<?> patchToH2DB(@RequestBody PersonH2 personH2) {
        log.info("patchToH2DB(" + personH2.toString() + ")... CALLED!");
        PersonH2 person = h2JPAService.getPersonH2(personH2.getId());
        person.setLastUpdated(new Date());
        person.setName(personH2.getName());
        person.setAge(personH2.getAge());
        return h2JPAService.postOrUpdatePersonToH2(person);
    }

    @DeleteMapping("/jpa/h2/{id}")
    public ResponseEntity<?> deleteFromH2DB(@PathVariable("id") long id) {
        log.info("deleteFromH2DB(" + id + ")... CALLED!");
        return h2JPAService.deletePersonFromH2(id);
    }
}
