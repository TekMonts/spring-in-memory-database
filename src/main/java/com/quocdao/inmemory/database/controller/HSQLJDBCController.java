package com.quocdao.inmemory.database.controller;

import com.quocdao.inmemory.database.entity.PersonHSQL;
import com.quocdao.inmemory.database.service.HSQLJDBCService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@Slf4j
public class HSQLJDBCController {

    @Autowired
    private HSQLJDBCService hsqljdbcService;

    @GetMapping("/jdbc/hsql/{id}")
    public ResponseEntity<?> getByIDFromHSQLDB(@PathVariable("id") long id) {
        log.info("getByIDFromHSQLDB(" + id + ")... CALLED!");
        return hsqljdbcService.getPersonUsingHSQL(id);
    }

    @GetMapping("/jdbc/hsql")
    public ResponseEntity<?> geAllFromHSQLDB() {
        log.info("geAllFromHSQLDB()... CALLED!");
        return hsqljdbcService.getAllPersonUsingHSQL();
    }

    @PostMapping("/jdbc/hsql")
    public ResponseEntity<?> postToHSQLDB(@RequestBody PersonHSQL personHSQL) {
        log.info("postToHSQLDB(" + personHSQL.toString() + ")... CALLED!");
        return hsqljdbcService.postPersonToHSQL(personHSQL);
    }

    @PatchMapping("/jdbc/hsql")
    public ResponseEntity<?> patchToHSQLDB(@RequestBody PersonHSQL personHSQL) {
        log.info("patchToHSQLDB(" + personHSQL.toString() + ")... CALLED!");
        return hsqljdbcService.updatePersononHSQL(personHSQL);
    }

    @DeleteMapping("/jdbc/hsql/{id}")
    public ResponseEntity<?> deleteFromHSQLDB(@PathVariable("id") long id) {
        log.info("deleteFromHSQLDB(" + id + ")... CALLED!");
        return hsqljdbcService.deletePersonFromHSQL(id);
    }
}
