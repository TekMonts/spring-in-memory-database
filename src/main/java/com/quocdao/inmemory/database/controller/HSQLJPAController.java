package com.quocdao.inmemory.database.controller;

import com.quocdao.inmemory.database.entity.PersonHSQL;
import com.quocdao.inmemory.database.service.HSQLJPAService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
@Slf4j
public class HSQLJPAController {

    @Autowired
    private HSQLJPAService HSQLJPAService;

    @GetMapping("/jpa/hsql/{id}")
    public ResponseEntity<?> getByIDFromHSQLDB(@PathVariable("id") long id) {
        log.info("getByIDFromHSQLDB(" + id + ")... CALLED!");
        return HSQLJPAService.getPersonUsingHSQL(id);
    }

    @GetMapping("/jpa/hsql")
    public ResponseEntity<?> geAllFromHSQLDB() {
        log.info("geAllFromHSQLDB()... CALLED!");
        return HSQLJPAService.getAllPersonUsingHSQL();
    }

    @PostMapping("/jpa/hsql")
    public ResponseEntity<?> postToHSQLDB(@RequestBody PersonHSQL personHSQL) {
        log.info("postToHSQLDB(" + personHSQL.toString() + ")... CALLED!");
        personHSQL.setCreateTime(new Date());
        personHSQL.setLastUpdated(new Date());
        return HSQLJPAService.postOrUpdatePersonToHSQL(personHSQL);
    }

    @PatchMapping("/jpa/hsql")
    public ResponseEntity<?> patchToHSQLDB(@RequestBody PersonHSQL personHSQL) {
        log.info("patchToHSQLDB(" + personHSQL.toString() + ")... CALLED!");
        PersonHSQL person = HSQLJPAService.getPersonHSQL(personHSQL.getId());
        person.setLastUpdated(new Date());
        person.setName(personHSQL.getName());
        person.setAge(personHSQL.getAge());
        return HSQLJPAService.postOrUpdatePersonToHSQL(person);
    }

    @DeleteMapping("/jpa/hsql/{id}")
    public ResponseEntity<?> deleteFromHSQLDB(@PathVariable("id") long id) {
        log.info("deleteFromHSQLDB(" + id + ")... CALLED!");
        return HSQLJPAService.deletePersonFromHSQL(id);
    }
}
