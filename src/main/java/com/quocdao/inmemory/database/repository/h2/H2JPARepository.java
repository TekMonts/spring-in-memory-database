package com.quocdao.inmemory.database.repository.h2;

import com.quocdao.inmemory.database.entity.PersonH2;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface H2JPARepository extends JpaRepository<PersonH2, Long> {
}
