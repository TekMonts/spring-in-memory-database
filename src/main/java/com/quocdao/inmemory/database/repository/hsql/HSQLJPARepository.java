package com.quocdao.inmemory.database.repository.hsql;

import com.quocdao.inmemory.database.entity.PersonHSQL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HSQLJPARepository extends JpaRepository<PersonHSQL, Long> {
}
