package com.quocdao.inmemory.database.repository.h2;

import com.quocdao.inmemory.database.entity.PersonH2;
import com.quocdao.inmemory.database.entity.PersonH2RowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public class H2JDBCRepository {
    @Autowired
    @Qualifier("h2JdbcTemplate")
    private final JdbcTemplate h2JdbcTemplate;

    public H2JDBCRepository(JdbcTemplate h2JdbcTemplate) {
        this.h2JdbcTemplate = h2JdbcTemplate;
    }

    public Optional<PersonH2> findById(long id) {
        String sql = "SELECT id, name, age, dbType, createTime, lastUpdated FROM PersonH2 WHERE id = ?;";
        return h2JdbcTemplate.query(sql, new PersonH2RowMapper(), id)
                .stream()
                .findFirst();
    }

    public int addPersonH2JDBC(PersonH2 personH2) {
        String sql = "INSERT into PersonH2(name, age, dbType, createTime, lastUpdated) VALUES (?,?,?,?,?);";
        return h2JdbcTemplate.update(sql, personH2.getName(),  personH2.getAge(), "H2-JDBC", new Date(), new Date());
    }

    public List<PersonH2> findAll() {
        String sql = " SELECT id, name, age, dbType, createTime, lastUpdated FROM PersonH2 order by id desc";
        return h2JdbcTemplate.query(sql, new PersonH2RowMapper());
    }

    public int deleteById(long id) {
        String sql = "DELETE FROM PersonH2 WHERE id = ?;";
        return h2JdbcTemplate.update(sql, id);
    }

    public int updatePersonH2JDBC(PersonH2 personH2) {
        String sql = "UPDATE PersonH2 SET name = ?, age = ?, lastUpdated = ? WHERE id =?;";
        return h2JdbcTemplate.update(sql, personH2.getName(), personH2.getAge(), new Date(), personH2.getId());
    }
}
