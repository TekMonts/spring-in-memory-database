package com.quocdao.inmemory.database.repository.hsql;

import com.quocdao.inmemory.database.entity.PersonHSQL;
import com.quocdao.inmemory.database.entity.PersonHSQLRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public class HSQLJDBCRepository {
    @Autowired
    @Qualifier("hsqlJdbcTemplate")
    private final JdbcTemplate hsqlJdbcTemplate;

    public HSQLJDBCRepository(JdbcTemplate hsqlJdbcTemplate) {
        this.hsqlJdbcTemplate = hsqlJdbcTemplate;
    }

    public Optional<PersonHSQL> findById(long id) {
        String sql = "SELECT id, name, age, dbType, createTime, lastUpdated FROM PersonHSQL WHERE id = ?;";
        return hsqlJdbcTemplate.query(sql, new PersonHSQLRowMapper(), id)
                .stream()
                .findFirst();
    }

    public int addPersonHSQLJDBC(PersonHSQL personH2) {
        String sql = "INSERT into PersonHSQL(name, age, dbType, createTime, lastUpdated) VALUES (?,?,?,?,?);";
        return hsqlJdbcTemplate.update(sql, personH2.getName(),  personH2.getAge(), "HSQL-JDBC", new Date(), new Date());
    }

    public List<PersonHSQL> findAll() {
        String sql = " SELECT id, name, age, dbType, createTime, lastUpdated FROM PersonHSQL order by id desc";
        return hsqlJdbcTemplate.query(sql, new PersonHSQLRowMapper());
    }

    public int deleteById(long id) {
        String sql = "DELETE FROM PersonHSQL WHERE id = ?;";
        return hsqlJdbcTemplate.update(sql, id);
    }

    public int updatePersonHSQLJDBC(PersonHSQL personH2) {
        String sql = "UPDATE PersonHSQL SET name = ?, age = ?, lastUpdated = ? WHERE id =?;";
        return hsqlJdbcTemplate.update(sql, personH2.getName(), personH2.getAge(), new Date(), personH2.getId());
    }
}
