package com.quocdao.inmemory.database.entity;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonH2RowMapper implements RowMapper<PersonH2> {
    @Override
    public PersonH2 mapRow(ResultSet rs, int rowNum) throws SQLException {
        return PersonH2.builder()
                .id(rs.getLong("id"))
                .name(rs.getString("name"))
                .age(rs.getInt("age"))
                .dbType(rs.getString("dbType"))
                .createTime(rs.getTimestamp("createTime"))
                .lastUpdated(rs.getTimestamp("lastUpdated"))
                .build();
    }
}
