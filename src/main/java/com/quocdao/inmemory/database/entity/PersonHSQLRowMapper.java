package com.quocdao.inmemory.database.entity;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonHSQLRowMapper implements RowMapper<PersonHSQL> {
    @Override
    public PersonHSQL mapRow(ResultSet rs, int rowNum) throws SQLException {
        return PersonHSQL.builder()
                .id(rs.getLong("id"))
                .name(rs.getString("name"))
                .age(rs.getInt("age"))
                .dbType(rs.getString("dbType"))
                .createTime(rs.getTimestamp("createTime"))
                .lastUpdated(rs.getTimestamp("lastUpdated"))
                .build();
    }
}
