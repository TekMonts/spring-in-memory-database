package com.quocdao.inmemory.database.configuration;

import com.quocdao.inmemory.database.entity.PersonHSQL;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Objects;


@Configuration
@EnableTransactionManagement
@EnableAutoConfiguration
@EnableJpaRepositories(
        basePackages = {"com.quocdao.inmemory.database.repository.hsql"},
        entityManagerFactoryRef = "hsqlEntityManagerFactory",
        transactionManagerRef = "hsqlTransactionManager"
)
public class HSQLJpaConfiguration {

    @Bean
    @ConfigurationProperties("spring.datasource.hsql")
    public DataSourceProperties hsqlJPADataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean(name = "hsqlJPADataSource")
    public DataSource hsqlJPADataSource() {
        return hsqlJPADataSourceProperties()
                .initializeDataSourceBuilder()
                .build();
    }

    @Bean(name = "hsqlEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean hsqlEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(hsqlJPADataSource())
                .packages(PersonHSQL.class)
                .build();
    }

    @Bean(name = "hsqlTransactionManager")
    public PlatformTransactionManager hsqlTransactionManager(
            @Qualifier("hsqlEntityManagerFactory") LocalContainerEntityManagerFactoryBean hsqlEntityManagerFactory) {
        return new JpaTransactionManager(Objects.requireNonNull(hsqlEntityManagerFactory.getObject()));
    }

}