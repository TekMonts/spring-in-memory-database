package com.quocdao.inmemory.database.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class MultipleDataSourcesConfiguration {

    @Bean(name = "h2JDBCDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.h2")
    public DataSource h2JDBCDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "h2JdbcTemplate")
    public JdbcTemplate h2JdbcTemplate(@Qualifier("h2JDBCDataSource") DataSource h2JDBCDataSource) {
        return new JdbcTemplate(h2JDBCDataSource);
    }

    @Bean(name = "hsqlJDBCDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.hsql")
    public DataSource hsqlJDBCDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "hsqlJdbcTemplate")
    public JdbcTemplate hsqlJdbcTemplate(@Qualifier("hsqlJDBCDataSource") DataSource hsqlJDBCDataSource) {
        return new JdbcTemplate(hsqlJDBCDataSource);
    }
}
