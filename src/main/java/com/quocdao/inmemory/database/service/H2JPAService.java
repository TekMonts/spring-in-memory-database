package com.quocdao.inmemory.database.service;

import com.quocdao.inmemory.database.entity.PersonH2;
import com.quocdao.inmemory.database.repository.h2.H2JPARepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional("transactionManager")
@Slf4j
public class H2JPAService {
    @Autowired
    private H2JPARepository h2JPARepository;

    public PersonH2 getPersonH2(long id) {
        return h2JPARepository.getById(id);
    }

    public ResponseEntity<?> getPersonUsingH2(long id) {
        try {
            PersonH2 personH2 = h2JPARepository.findById(id).orElse(null);

            if (personH2 != null) {
                return new ResponseEntity<>(personH2, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> getAllPersonUsingH2() {
        try {
            List<PersonH2> personH2List = h2JPARepository.findAll();
            if (!personH2List.isEmpty()) {
                return new ResponseEntity<>(personH2List, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> postOrUpdatePersonToH2(PersonH2 personH2) {
        try {
           PersonH2 ps = h2JPARepository.saveAndFlush(personH2);
            return new ResponseEntity<>(ps, HttpStatus.OK);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> deletePersonFromH2(long id) {
        try {
            h2JPARepository.deleteById(id);
            return new ResponseEntity<>(id, HttpStatus.OK);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
