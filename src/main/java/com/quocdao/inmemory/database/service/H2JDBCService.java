package com.quocdao.inmemory.database.service;

import com.quocdao.inmemory.database.entity.PersonH2;
import com.quocdao.inmemory.database.repository.h2.H2JDBCRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class H2JDBCService {
    @Autowired
    private H2JDBCRepository h2JDBCRepository;

    public ResponseEntity<?> getPersonUsingH2(long id) {
        try {
            PersonH2 personH2 = h2JDBCRepository.findById(id).orElse(null);

            if (personH2 != null) {
                return new ResponseEntity<>(personH2, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> getAllPersonUsingH2() {
        try {
            List<PersonH2> personH2List = h2JDBCRepository.findAll();
            if (!personH2List.isEmpty()) {
                return new ResponseEntity<>(personH2List, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> postPersonToH2(PersonH2 personH2) {
        try {
            if (h2JDBCRepository.addPersonH2JDBC(personH2) > 0) {
                return new ResponseEntity<>(h2JDBCRepository.findAll().stream().findFirst(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> updatePersononH2(PersonH2 personH2) {
        try {
            if (h2JDBCRepository.updatePersonH2JDBC(personH2) > 0) {
                return new ResponseEntity<>(h2JDBCRepository.findById(personH2.getId()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> deletePersonFromH2(long id) {
        try {
            if (h2JDBCRepository.deleteById(id) > 0) {
                return new ResponseEntity<>(id, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
