package com.quocdao.inmemory.database.service;

import com.quocdao.inmemory.database.entity.PersonHSQL;
import com.quocdao.inmemory.database.repository.hsql.HSQLJDBCRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class HSQLJDBCService {
    @Autowired
    private HSQLJDBCRepository hsqlJDBCRepository;

    public ResponseEntity<?> getPersonUsingHSQL(long id) {
        try {
            PersonHSQL personHSQL = hsqlJDBCRepository.findById(id).orElse(null);

            if (personHSQL != null) {
                return new ResponseEntity<>(personHSQL, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> getAllPersonUsingHSQL() {
        try {
            List<PersonHSQL> personHSQLList = hsqlJDBCRepository.findAll();
            if (!personHSQLList.isEmpty()) {
                return new ResponseEntity<>(personHSQLList, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> postPersonToHSQL(PersonHSQL personHSQL) {
        try {
            if (hsqlJDBCRepository.addPersonHSQLJDBC(personHSQL) > 0) {
                return new ResponseEntity<>(hsqlJDBCRepository.findAll().stream().findFirst(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> updatePersononHSQL(PersonHSQL personHSQL) {
        try {
            if (hsqlJDBCRepository.updatePersonHSQLJDBC(personHSQL) > 0) {
                return new ResponseEntity<>(hsqlJDBCRepository.findById(personHSQL.getId()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> deletePersonFromHSQL(long id) {
        try {
            if (hsqlJDBCRepository.deleteById(id) > 0) {
                return new ResponseEntity<>(id, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
