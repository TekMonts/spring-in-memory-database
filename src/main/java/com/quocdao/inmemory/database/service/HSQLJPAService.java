package com.quocdao.inmemory.database.service;

import com.quocdao.inmemory.database.entity.PersonHSQL;
import com.quocdao.inmemory.database.repository.hsql.HSQLJPARepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional("hsqlTransactionManager")
@Slf4j
public class HSQLJPAService {
    @Autowired
    private HSQLJPARepository HSQLJPARepository;

    public ResponseEntity<?> getPersonUsingHSQL(long id) {
        try {
            PersonHSQL personHSQL = HSQLJPARepository.findById(id).orElse(null);

            if (personHSQL != null) {
                return new ResponseEntity<>(personHSQL, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> getAllPersonUsingHSQL() {
        try {
            List<PersonHSQL> personHSQLList = HSQLJPARepository.findAll();

            if (!personHSQLList.isEmpty()) {
                return new ResponseEntity<>(personHSQLList, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> postOrUpdatePersonToHSQL(PersonHSQL personHSQL) {
        try {
           PersonHSQL ps = HSQLJPARepository.saveAndFlush(personHSQL);
            return new ResponseEntity<>(ps, HttpStatus.OK);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> deletePersonFromHSQL(long id) {
        try {
            HSQLJPARepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public PersonHSQL getPersonHSQL(long id) {
        return  HSQLJPARepository.getById(id);
    }
}
