<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<header>
    <title>In-Memory Database example by Quoc DAO</title>
    <style>
        .left-column {
            width: 50%;
            height: 380px;
            float: left;
        }
        .right-column {
            width: 50%;
            height: 380px;
            float: right;
        }
        hr {
            width: 50%;
            margin-left: 0;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>
        let dburltype = 'jpa';
        $(document).ready(function () {
            $("input[name='accessdb']").change(function(){
                dburltype = $(this).val();
                console.log("Using database type: " + dburltype);
            });

            $("#btnh2g").on("click", function (e) {
                e.preventDefault();
                if (!$("#h2IdInput").val()) {
                    alert("Please input id to search!");
                    $("#h2IdInput").focus();
                    return false;
                }
                $.ajax({
                    url: "/" + dburltype + "/h2/" + $("#h2IdInput").val(),
                    type: "GET",
                    contentType: "application/json; charset=UTF-8",
                    dataType: "json",
                    success: function (data) {
                        if(data === undefined) {
                            $("#respstatus").html("No data found for given id " + $("#h2IdInput").val() + "!");
                            $("#resp").html("");
                        } else {
                            $("#respstatus").html("Found 1 record:");
                            $("#resp").html("<table> <thead> <tr> <th>ID</th> <th>Name</th> <th>Age</th> <th>DB Type</th> <th>Last Updated</th> <th>Created Date</th> <th>Action</th> </tr></thead> <tbody> <tr id='h2rid" + data.id + "'> <td>"+ data.id +"</td><td>"+ data.name +"</td><td>" + data.age +"</td><td>"+ data.dbType +"</td><td>"+ data.createTime +"</td><td>"+ data.lastUpdated +"</td><td> <input type='button' name='btnh2u' value='Update' onclick='updateh2("+ data.id + ", \""+ data.name + "\", "+ data.age +")'/> &emsp; <input type='button' name='btnh2d' value='Delete' onclick='deleteh2("+ data.id + ")'/> </td></tr></tbody> </table>");
                        }
                    },
                    error: function (data) {
                        $("#respstatus").html("Failed to find the record from DB with given id " + $("#h2IdInput").val());
                        $("#resp").html("ERROR: " + data.responseText);
                    },
                });
            });

            $("#btnh2ga").on("click", function (e) {
                e.preventDefault();
                $.ajax({
                    url: "/" + dburltype + "/h2",
                    type: "GET",
                    contentType: "application/json; charset=UTF-8",
                    dataType: "json",
                    success: function (data) {
                        if(data === undefined) {
                            $("#respstatus").html("No data found in H2 DB!");
                            $("#resp").html("");
                        } else {
                            var count = 0;
                            var html = "<table> <thead> <tr> <th>ID</th> <th>Name</th> <th>Age</th> <th>DB Type</th> <th>Created Date</th> <th>Last Updated</th> <th>Action</th> </tr></thead> <tbody> ";
                            for (var dt in data) {
                                count ++;
                                html += "<tr id='h2rid" + data[dt].id + "'> <td>"+ data[dt].id +"</td><td>"+ data[dt].name +"</td><td>"+ data[dt].age +"</td><td>"+ data[dt].dbType +"</td><td>"+ data[dt].createTime +"</td><td>"+ data[dt].lastUpdated +"</td><td> <input type='button' name='btnh2u' value='Update' onclick='updateh2("+ data[dt].id + ", \""+ data[dt].name + "\", "+ data[dt].age +")'/> &emsp; <input type='button' name='btnh2d' value='Delete' onclick='deleteh2("+ data[dt].id + ")'/> </td></tr>";
                            }
                            html += " </tbody> </table>"
                            $("#respstatus").html("Found " + count + " records:");
                            $("#resp").html(html);
                        }
                    },
                    error: function (data) {
                        $("#resp").html("ERROR: " + data.responseText);
                    },
                });
            });

            $("#btnh2pu").on("click", function (e) {
                e.preventDefault();
                if($("#idh2").val()){
                    $.ajax({
                        url: "/" + dburltype + "/h2",
                        type: "PATCH",
                        data: "{ \"id\": " + $("#idh2").val() +", \"name\": \"" + $("#h2name").val() + "\", \"age\": " + $("#h2age").val() + "}",
                        contentType: "application/json; charset=UTF-8",
                        dataType: "json",
                        success: function (data) {
                            $("#respstatus").html("Update record on H2 DB successful!");
                            $("#h2rid" + $("#idh2").val()).html("<td>"+ data.id +"</td><td>"+ data.name +"</td><td>"+ data.age +"</td><td>"+ data.dbType +"</td><td>"+ data.createTime +"</td><td>"+ data.lastUpdated +"</td><td> <input type='button' name='btnh2u' value='Update' onclick='updateh2("+ data.id + ", \""+ data.name + "\", "+ data.age +")'/> &emsp; <input type='button' name='btnh2d' value='Delete' onclick='deleteh2("+ data.id + ")'/> </td>");
                        },
                        error: function (data) {
                            $("#respstatus").html("Update record on H2 DB failed!");
                            $("#resp").html("ERROR: " + data.responseText);
                        },
                    });
                } else {
                    $.ajax({
                        url: "/" + dburltype + "/h2",
                        type: "POST",
                        data: "{ \"name\": \"" + $("#h2name").val() + "\", \"age\": " + $("#h2age").val() + "}",
                        contentType: "application/json; charset=UTF-8",
                        dataType: "json",
                        success: function (data) {
                            $("#respstatus").html("Post to H2 DB successful!");
                            $("#resp").html("<table> <thead> <tr> <th>ID</th> <th>Name</th> <th>Age</th> <th>DB Type</th> <th>Created Date</th> <th>Last Updated</th> <th>Action</th> </tr></thead> <tbody> <tr id='h2rid" + data.id + "'> <td>"+ data.id +"</td><td>"+ data.name +"</td><td>"+ data.age +"</td><td>"+ data.dbType +"</td><td>"+ data.createTime +"</td><td>"+ data.lastUpdated +"</td><td> <input type='button' name='btnh2u' value='Update' onclick='updateh2("+ data.id + ", \""+ data.name + "\", "+ data.age +")'/> &emsp; <input type='button' name='btnh2d' value='Delete' onclick='deleteh2("+ data.id + ")'/> </td></tr></tbody> </table>");
                        },
                        error: function (data) {
                            $("#respstatus").html("Post to H2 DB failed!");
                            $("#resp").html("ERROR: " + data.responseText);
                        },
                    });
                }
            });
            $("#btnhsqlg").on("click", function (e) {
                e.preventDefault();
                if (!$("#hsqlIdInput").val()) {
                    alert("Please input id to search!");
                    $("#hsqlIdInput").focus();
                    return false;
                }
                $.ajax({
                    url: "/" + dburltype + "/hsql/" + $("#hsqlIdInput").val(),
                    type: "GET",
                    contentType: "application/json; charset=UTF-8",
                    dataType: "json",
                    success: function (data) {
                        if(data === undefined) {
                            $("#respstatus").html("No data found for given id " + $("#hsqlIdInput").val() + "!");
                            $("#resp").html("");
                        } else {
                            $("#respstatus").html("Found 1 record:");
                            $("#resp").html("<table> <thead> <tr> <th>ID</th> <th>Name</th> <th>Age</th> <th>DB Type</th> <th>Last Updated</th> <th>Created Date</th> <th>Action</th> </tr></thead> <tbody> <tr id='hsqlrid" + data.id + "'> <td>"+ data.id +"</td><td>"+ data.name +"</td><td>" + data.age +"</td><td>"+ data.dbType +"</td><td>"+ data.createTime +"</td><td>"+ data.lastUpdated +"</td><td> <input type='button' name='btnhsqlu' value='Update' onclick='updatehsql("+ data.id + ", \""+ data.name + "\", "+ data.age +")'/> &emsp; <input type='button' name='btnhsqld' value='Delete' onclick='deletehsql("+ data.id + ")'/> </td></tr></tbody> </table>");
                        }
                    },
                    error: function (data) {
                        $("#respstatus").html("Failed to find the record from DB with given id " + $("#hsqlIdInput").val());
                        $("#resp").html("ERROR: " + data.responseText);
                    },
                });
            });

            $("#btnhsqlga").on("click", function (e) {
                e.preventDefault();
                $.ajax({
                    url: "/" + dburltype + "/hsql",
                    type: "GET",
                    contentType: "application/json; charset=UTF-8",
                    dataType: "json",
                    success: function (data) {
                        if(data === undefined) {
                            $("#respstatus").html("No data found in HSQL DB!");
                            $("#resp").html("");
                        } else {
                            var count = 0;
                            var html = "<table> <thead> <tr> <th>ID</th> <th>Name</th> <th>Age</th> <th>DB Type</th> <th>Created Date</th> <th>Last Updated</th> <th>Action</th> </tr></thead> <tbody> ";
                            for (var dt in data) {
                                count ++;
                                html += "<tr id='hsqlrid" + data[dt].id + "'> <td>"+ data[dt].id +"</td><td>"+ data[dt].name +"</td><td>"+ data[dt].age +"</td><td>"+ data[dt].dbType +"</td><td>"+ data[dt].createTime +"</td><td>"+ data[dt].lastUpdated +"</td><td> <input type='button' name='btnhsqlu' value='Update' onclick='updatehsql("+ data[dt].id + ", \""+ data[dt].name + "\", "+ data[dt].age +")'/> &emsp; <input type='button' name='btnhsqld' value='Delete' onclick='deletehsql("+ data[dt].id + ")'/> </td></tr>";
                            }
                            html += " </tbody> </table>"
                            $("#respstatus").html("Found " + count + " records:");
                            $("#resp").html(html);
                        }
                    },
                    error: function (data) {
                        $("#resp").html("ERROR: " + data.responseText);
                    },
                });
            });

            $("#btnhsqlpu").on("click", function (e) {
                e.preventDefault();
                if($("#idhsql").val()){
                    $.ajax({
                        url: "/" + dburltype + "/hsql",
                        type: "PATCH",
                        data: "{ \"id\": " + $("#idhsql").val() +", \"name\": \"" + $("#hsqlname").val() + "\", \"age\": " + $("#hsqlage").val() + "}",
                        contentType: "application/json; charset=UTF-8",
                        dataType: "json",
                        success: function (data) {
                            $("#respstatus").html("Update record on HSQL DB successful!");
                            $("#hsqlrid" + $("#idhsql").val()).html("<td>"+ data.id +"</td><td>"+ data.name +"</td><td>"+ data.age +"</td><td>"+ data.dbType +"</td><td>"+ data.createTime +"</td><td>"+ data.lastUpdated +"</td><td> <input type='button' name='btnhsqlu' value='Update' onclick='updatehsql("+ data.id + ", \""+ data.name + "\", "+ data.age +")'/> &emsp; <input type='button' name='btnhsqld' value='Delete' onclick='deletehsql("+ data.id + ")'/> </td>");
                        },
                        error: function (data) {
                            $("#respstatus").html("Update record on HSQL DB failed!");
                            $("#resp").html("ERROR: " + data.responseText);
                        },
                    });
                } else {
                    $.ajax({
                        url: "/" + dburltype + "/hsql",
                        type: "POST",
                        data: "{ \"name\": \"" + $("#hsqlname").val() + "\", \"age\": " + $("#hsqlage").val() + "}",
                        contentType: "application/json; charset=UTF-8",
                        dataType: "json",
                        success: function (data) {
                            $("#respstatus").html("Post to HSQL DB successful!");
                            $("#resp").html("<table> <thead> <tr> <th>ID</th> <th>Name</th> <th>Age</th> <th>DB Type</th> <th>Created Date</th> <th>Last Updated</th> <th>Action</th> </tr></thead> <tbody> <tr id='hsqlrid" + data.id + "'> <td>"+ data.id +"</td><td>"+ data.name +"</td><td>"+ data.age +"</td><td>"+ data.dbType +"</td><td>"+ data.createTime +"</td><td>"+ data.lastUpdated +"</td><td> <input type='button' name='btnhsqlu' value='Update' onclick='updatehsql("+ data.id + ", \""+ data.name + "\", "+ data.age +")'/> &emsp; <input type='button' name='btnhsqld' value='Delete' onclick='deletehsql("+ data.id + ")'/> </td></tr></tbody> </table>");
                        },
                        error: function (data) {
                            $("#respstatus").html("Post to HSQL DB failed!");
                            $("#resp").html("ERROR: " + data.responseText);
                        },
                    });
                }
            });

        });
        function updateh2(id, name, age) {
            $("#idh2").val(id);
            $("#h2name").val(name);
            $("#h2age").val(age);
            $("#btnh2pu").val("Update Person");
            $("input[name='accessdb']").attr("disabled", true);
            $("#h2name").focus();
        }

        function deleteh2(id) {
            if(confirm("Are you sure to delete this record?\nRecord id: " + id)) {
                $.ajax({
                    url: "/" + dburltype + "/h2/" + id,
                    type: "DELETE",
                    success: function (data) {
                        $("#respstatus").html("Delete record with id " + id + " successful!");
                        document.getElementById("h2rid" + id).remove();
                    },
                    error: function (data) {
                        $("#respstatus").html("Delete record with id " + id + " failed!");
                        $("#resp").html("ERROR: " + data.responseText);
                    },
                });
            }
        }
        function updatehsql(id, name, age) {
            $("#idhsql").val(id);
            $("#hsqlname").val(name);
            $("#hsqlage").val(age);
            $("#btnhsqlpu").val("Update Person");
            $("input[name='accessdb']").attr("disabled", true);
            $("#hsqlname").focus();
        }

        function deletehsql(id) {
            if(confirm("Are you sure to delete this record?\nRecord id: " + id)) {
                $.ajax({
                    url: "/" + dburltype + "/hsql/" + id,
                    type: "DELETE",
                    success: function (data) {
                        $("#respstatus").html("Delete record with id " + id + " successful!");
                        document.getElementById("hsqlrid" + id).remove();
                    },
                    error: function (data) {
                        $("#respstatus").html("Delete record with id " + id + " failed!");
                            $("#resp").html("ERROR: " + data.responseText);
                    },
                });
            }
        }
    </script>
</header>
<body>
<div style="margin-left: 25%;">
    <form>
        Please choose how to access the database:
        <input type="radio" id="jpa" name="accessdb" value="jpa" checked>
        <label for="jpa">JPA</label>
        <input type="radio" id="jdbc" name="accessdb" value="jdbc">
        <label for="jdbc">JDBC</label>
    </form>
</div>
<div class="left-column">
    <h3>H2 In-Memory Database</h3>
    <h4 style="margin-left: 5%;">GET</h4>
    <form style="margin-left: 10%;">
        ID: <input placeholder="Id to search from H2 DB" id="h2IdInput" name="h2IdInput" type="number" min="1" step="1"  />
        <br><br>
        <input type="button" id="btnh2g" name="btnh2g" value="Search By ID" />
        &emsp;
        <input type="button" id="btnh2ga" name="btnh2ga" value="Search All" />
    </form>
    <hr>
    <h4 style="margin-left: 5%;">POST/UPDATE</h4>
    <form style="margin-left: 10%;">
        Name: <input type="text" placeholder="Enter name" name="h2name" id="h2name" required="required" />
        <br><br>
        Age: &ensp; <input type="number" style="width: 170px;" placeholder="Enter age" min="1" max="105" step="1" name="h2age" id="h2age" required="required" />
        <input type="hidden" id="idh2" name="idh2" />
        <br><br>
        <input type="button" id="btnh2pu" name="btnh2pu" value="Add Person" />
        &emsp;
        <button type="reset" name="h2reset" value="Reset" onclick="$('#btnh2pu').val('Add Person'); $('#idh2').val(''); $('input[name=accessdb]').attr('disabled', false);">Reset</button>
    </form>
</div>
<div class="right-column">
    <h3>HSQL In-Memory Database</h3>
    <h4 style="margin-left: 5%;">GET</h4>
    <form style="margin-left: 10%;">
        ID: <input placeholder="Id to search from HSQL DB" id="hsqlIdInput" name="hsqlIdInput" type="number" min="1" step="1" />
        <br><br>
        <input type="button" id="btnhsqlg" name="btnhsqlg" value="Search By ID" />
        &emsp;
        <input type="button" id="btnhsqlga" name="btnhsqlga" value="Search All" />
    </form>
    <hr>
    <h4 style="margin-left: 5%;">POST/UPDATE</h4>
    <form style="margin-left: 10%;">
        Name: <input type="text" placeholder="Enter name" name="hsqlname" id="hsqlname" required="required" />
        <br><br>
        Age: &ensp; <input type="number" style="width: 170px;" placeholder="Enter age" min="1" max="105" step="1" name="hsqlage" id="hsqlage" required="required" />
        <input type="hidden" id="idhsql" name="idhsql" />
        <br><br>
        <input type="button" id="btnhsqlpu" name="btnhsqlpu" value="Add Person" />
        &emsp;
        <button type="reset" name="hsqlreset" value="Reset" onclick="$('#btnhsqlpu').val('Add Person'); $('#idhsql').val(''); $('input[name=accessdb]').attr('disabled', false);">Reset</button>
    </form>
</div>
<hr style="margin: auto">
<p id="respstatus">
</p>
<div id="resp">
</div>
</body>
</html>
